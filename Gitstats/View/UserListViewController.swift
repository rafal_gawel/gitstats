//
//  UserListViewController.swift
//  Gitstats
//
//  Created by Rafał Gaweł on 29/01/2017.
//  Copyright © 2017 Rafał Gaweł. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class UserListViewController: UIViewController {

    @IBOutlet fileprivate var tableView: UITableView!
    @IBOutlet fileprivate var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet fileprivate var errorLabel: UILabel!

    fileprivate var viewModel: UserListViewModel?
    fileprivate let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()

        viewModel = UserListViewModel(resourceProvider: GithubResourceProvider())
        viewModel?.fetch()

        setupTitle()
        setupNavigationItem()
        setupRx()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)

        if let reposVC = segue.destination as? RepoListViewController,
            let indexPath = sender as? IndexPath {
            reposVC.viewModel = viewModel?.items.value?[indexPath.row].repoList
        }
    }

    fileprivate func setupTitle() {
        title = viewModel?.titleText
    }

    fileprivate func setupNavigationItem() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: nil)
    }

    fileprivate func setupRx() {
        guard let viewModel = viewModel else {
            return
        }

        viewModel.items.asObservable()
            .filter { $0 != nil }
            .map { $0! }
            .bindTo(tableView.rx.items(cellIdentifier: "UserCell", cellType: UserTableViewCell.self)) { (_, viewModel, cell) in
                cell.viewModel = viewModel
            }.addDisposableTo(disposeBag)

        tableView.rx.itemSelected.bindNext { [weak self] (indexPath) in
                self?.performSegue(withIdentifier: "showRepos", sender: indexPath)
                self?.tableView.deselectRow(at: indexPath, animated: true)
            }.addDisposableTo(disposeBag)

        viewModel.loadingState.loadingHidden.asObservable()
            .bindTo(loadingIndicator.rx.isHidden)
            .addDisposableTo(disposeBag)

        viewModel.loadingState.error.asObservable()
            .bindTo(errorLabel.rx.text)
            .addDisposableTo(disposeBag)

        viewModel.loadingState.dataViewHidden.asObservable()
            .bindTo(tableView.rx.isHidden)
            .addDisposableTo(disposeBag)

        navigationItem.rightBarButtonItem?.rx.tap
            .bindNext { [weak self] in
                self?.viewModel?.fetch()
            }.addDisposableTo(disposeBag)
    }

}
