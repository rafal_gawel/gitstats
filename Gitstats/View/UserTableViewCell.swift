//
//  UserTableViewCell.swift
//  Gitstats
//
//  Created by Rafał Gaweł on 29/01/2017.
//  Copyright © 2017 Rafał Gaweł. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Kingfisher

class UserTableViewCell: UITableViewCell {

    @IBOutlet fileprivate var userNameLabel: UILabel!
    @IBOutlet fileprivate var repositoriesLabel: UILabel!
    @IBOutlet fileprivate var avatarImageView: UIImageView!

    fileprivate var disposeBag = DisposeBag()

    var viewModel: UserViewModel? {

        didSet {
            disposeBag = DisposeBag()

            guard let viewModel = viewModel else {
                return
            }

            viewModel.userName.asObservable()
                .bindTo(userNameLabel.rx.text)
                .addDisposableTo(disposeBag)

            viewModel.avatarImageURL.asObservable()
                .bindNext { [weak self] (avatarImageURL) in
                    if let avatarImageURL = avatarImageURL, let url = URL(string: avatarImageURL) {
                        self?.avatarImageView.kf.setImage(with: url)
                    } else {
                        self?.avatarImageView.kf.cancelDownloadTask()
                        self?.avatarImageView.image = nil
                    }
                }.addDisposableTo(disposeBag)

            viewModel.repoList.loadingState.dataViewHidden.asObservable()
                .bindTo(repositoriesLabel.rx.isHidden)
                .addDisposableTo(disposeBag)

            viewModel.repoCountText.asObservable()
                .bindTo(repositoriesLabel.rx.text)
                .addDisposableTo(disposeBag)

            viewModel.repoList.fetchIfNotLoaded()
        }

    }

}
