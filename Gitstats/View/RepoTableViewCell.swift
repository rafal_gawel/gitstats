//
//  RepoTableViewCell.swift
//  Gitstats
//
//  Created by Rafał Gaweł on 31/01/2017.
//  Copyright © 2017 Rafał Gaweł. All rights reserved.
//

import UIKit
import RxSwift

class RepoTableViewCell: UITableViewCell {

    @IBOutlet fileprivate var repoNameLabel: UILabel!
    @IBOutlet fileprivate var languageStatsLabel: UILabel!

    fileprivate var disposeBag = DisposeBag()

    var viewModel: RepoViewModel? {

        didSet {
            disposeBag = DisposeBag()

            guard let viewModel = viewModel else {
                return
            }

            viewModel.name.asObservable()
                .bindTo(repoNameLabel.rx.text)
                .addDisposableTo(disposeBag)

            viewModel.languageStats.loadingState.dataViewHidden.asObservable()
                .bindTo(languageStatsLabel.rx.isHidden)
                .addDisposableTo(disposeBag)

            viewModel.languageStats.items.asObservable()
                .bindTo(languageStatsLabel.rx.text)
                .addDisposableTo(disposeBag)

            viewModel.languageStats.fetchIfNotLoaded()
        }

    }

}
