//
//  RepoListViewController.swift
//  Gitstats
//
//  Created by Rafał Gaweł on 31/01/2017.
//  Copyright © 2017 Rafał Gaweł. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class RepoListViewController: UIViewController {

    var viewModel: RepoListViewModel?

    @IBOutlet fileprivate var tableView: UITableView!
    @IBOutlet fileprivate var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet fileprivate var errorLabel: UILabel!

    fileprivate let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()

        viewModel?.fetchIfNotLoaded()

        setupTitle()
        setupNavigationItem()
        setupRx()
    }

    fileprivate func setupTitle() {
        title = viewModel?.titleText
    }

    fileprivate func setupNavigationItem() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: nil)
    }

    fileprivate func setupRx() {
        guard let viewModel = viewModel else {
            return
        }

        viewModel.items.asObservable()
            .filter { $0 != nil }
            .map { $0! }
            .bindTo(tableView.rx.items(cellIdentifier: "RepoCell", cellType: RepoTableViewCell.self)) { (_, viewModel, cell) in
                cell.viewModel = viewModel
            }.addDisposableTo(disposeBag)

        viewModel.loadingState.loadingHidden.asObservable()
            .bindTo(loadingIndicator.rx.isHidden)
            .addDisposableTo(disposeBag)

        viewModel.loadingState.error.asObservable()
            .bindTo(errorLabel.rx.text)
            .addDisposableTo(disposeBag)

        viewModel.loadingState.dataViewHidden.asObservable()
            .bindTo(tableView.rx.isHidden)
            .addDisposableTo(disposeBag)

        navigationItem.rightBarButtonItem?.rx.tap
            .bindNext { [weak self] in
                self?.viewModel?.fetch()
            }.addDisposableTo(disposeBag)
    }

}
