//
//  ResourceViewModel.swift
//  Gitstats
//
//  Created by Rafał Gaweł on 31/01/2017.
//  Copyright © 2017 Rafał Gaweł. All rights reserved.
//

import Foundation
import RxSwift

class ResourceViewModel<Resource, ViewModel> where ViewModel: Any {

    let loadingState = LoadingStateViewModel()
    var items: Variable<ViewModel?> = Variable(nil)

    func fetch() {
        if case .loading = loadingState.state {
            return
        }

        loadingState.state = .loading
        items.value = nil

        startFetching()
    }

    func fetchIfNotLoaded() {
        if loadingState.state != .success {
            fetch()
        }
    }

    func startFetching() {
        fatalError("Must override")
    }

    func fetchCallback(result: RequestResult<Resource>) {
        switch result {
        case .success(let data):
            populate(data: data)
            loadingState.state = .success
        case .failure(let error):
            loadingState.state = .error(message: error)
        }
    }

    func populate(data: Resource) {
        fatalError("Must override")
    }

}
