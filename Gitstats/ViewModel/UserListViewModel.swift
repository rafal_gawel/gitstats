//
//  UserListViewModel.swift
//  Gitstats
//
//  Created by Rafał Gaweł on 27/01/2017.
//  Copyright © 2017 Rafał Gaweł. All rights reserved.
//

import Foundation
import RxSwift

class UserListViewModel: ResourceViewModel<[User], [UserViewModel]> {

    var titleText: String {
        return "Users"
    }

    fileprivate let resourceProvider: ResourceProvider

    init(resourceProvider: ResourceProvider) {
        self.resourceProvider = resourceProvider
    }

    override func startFetching() {
        resourceProvider.fetchUsers { [weak self] (result) in
            self?.fetchCallback(result: result)
        }
    }

    override func populate(data: [User]) {
        items.value = data.map { UserViewModel(user: $0, resourceProvider: resourceProvider) }
    }

}
