//
//  LoadingState.swift
//  Gitstats
//
//  Created by Rafał Gaweł on 30/01/2017.
//  Copyright © 2017 Rafał Gaweł. All rights reserved.
//

import Foundation

enum LoadingState: Equatable {

    case notStarted
    case loading
    case success
    case error(message: String)

    public static func == (lhs: LoadingState, rhs: LoadingState) -> Bool {
        switch (lhs, rhs) {
        case (.notStarted, .notStarted):
            return true
        case (.loading, .loading):
            return true
        case (.success, .success):
            return true
        case (.error(let firstMessage), .error(let secondMessage)):
            return firstMessage == secondMessage
        default:
            return false
        }
    }
}
