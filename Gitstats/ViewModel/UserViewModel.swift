//
//  UserViewModel.swift
//  Gitstats
//
//  Created by Rafał Gaweł on 26/01/2017.
//  Copyright © 2017 Rafał Gaweł. All rights reserved.
//

import Foundation
import RxSwift

class UserViewModel: Equatable {

    let repoList: RepoListViewModel
    let userName: Variable<String>
    let avatarImageURL: Variable<String?>
    let repoCountText: Variable<String?>

    fileprivate let disposeBag = DisposeBag()

    init(user: User, resourceProvider: ResourceProvider) {
        repoList = RepoListViewModel(username: user.name, resourceProvider: resourceProvider)
        userName = Variable(user.name)
        avatarImageURL = Variable(user.avatarImageURL)
        repoCountText = Variable(nil)

        repoList.items.asObservable().bindNext { [weak self] (repos) in
            guard let repos = repos else {
                return
            }

            self?.repoCountText.value = "repos: \(repos.count)"
        }.addDisposableTo(disposeBag)
    }

    public static func == (lhs: UserViewModel, rhs: UserViewModel) -> Bool {
        return lhs.userName.value == rhs.userName.value &&
            lhs.avatarImageURL.value == rhs.avatarImageURL.value
    }

}
