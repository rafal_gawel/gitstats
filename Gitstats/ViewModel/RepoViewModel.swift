//
//  RepoViewModel.swift
//  Gitstats
//
//  Created by Rafał Gaweł on 31/01/2017.
//  Copyright © 2017 Rafał Gaweł. All rights reserved.
//

import Foundation
import RxSwift

class RepoViewModel: Equatable {

    let languageStats: LanguageStatsViewModel
    let name: Variable<String>

    init(username: String, repo: String, resourceProvider: ResourceProvider) {
        name = Variable(repo)
        languageStats = LanguageStatsViewModel(username: username, repo: repo, resourceProvider: resourceProvider)
    }

    public static func == (lhs: RepoViewModel, rhs: RepoViewModel) -> Bool {
        return lhs.name.value == rhs.name.value
    }

}
