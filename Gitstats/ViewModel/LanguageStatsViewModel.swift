//
//  LanguageStatsViewModel.swift
//  Gitstats
//
//  Created by Rafał Gaweł on 31/01/2017.
//  Copyright © 2017 Rafał Gaweł. All rights reserved.
//

import Foundation
import RxSwift

class LanguageStatsViewModel: ResourceViewModel<Languages, String> {

    fileprivate let username: String
    fileprivate let repo: String
    fileprivate let resourceProvider: ResourceProvider

    init(username: String, repo: String, resourceProvider: ResourceProvider) {
        self.username = username
        self.repo = repo
        self.resourceProvider = resourceProvider
    }

    override func startFetching() {
        resourceProvider.fetchLanguages(username: username, repo: repo) { [weak self] (result) in
            self?.fetchCallback(result: result)
        }
    }

    override func populate(data: Languages) {
        let totalBytesOfCode: Double = data.items.reduce(0.0) { $0 + $1.bytesOfCode }
        let languages = data.items.map { (item) -> String in
            var language = item.name

            if totalBytesOfCode > 0 {
                let percent = item.bytesOfCode * 100.0 / totalBytesOfCode
                let roundedPercent = lround(percent)
                language += " \(roundedPercent)%"
            }

            return language
        }
        items.value = languages.joined(separator: ", ")
    }

}
