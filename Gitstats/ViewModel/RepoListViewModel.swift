//
//  RepoListViewModel.swift
//  Gitstats
//
//  Created by Rafał Gaweł on 31/01/2017.
//  Copyright © 2017 Rafał Gaweł. All rights reserved.
//

import Foundation
import RxSwift

class RepoListViewModel: ResourceViewModel<[Repo], [RepoViewModel]> {

    var titleText: String {
        return "\(username)'s repos"
    }

    fileprivate let username: String
    fileprivate let resourceProvider: ResourceProvider

    init(username: String, resourceProvider: ResourceProvider) {
        self.username = username
        self.resourceProvider = resourceProvider
    }

    override func startFetching() {
        resourceProvider.fetchRepos(username: username) { [weak self] (result) in
            self?.fetchCallback(result: result)
        }
    }

    override func populate(data: [Repo]) {
        items.value = data.map { RepoViewModel(username: username, repo: $0.name, resourceProvider: resourceProvider) }
    }

}
