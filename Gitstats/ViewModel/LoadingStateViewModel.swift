//
//  LoadingStateViewModel.swift
//  Gitstats
//
//  Created by Rafał Gaweł on 30/01/2017.
//  Copyright © 2017 Rafał Gaweł. All rights reserved.
//

import Foundation
import RxSwift

class LoadingStateViewModel {

    var error: Variable<String?> = Variable(nil)
    var loadingHidden: Variable<Bool> = Variable(true)
    var dataViewHidden: Variable<Bool> = Variable(true)

    var state: LoadingState = .notStarted {
        didSet {
            setupLoadingHiddenBasedOnState()
            setupDataViewHiddenBasedOnState()
            setupErrorBasedOnState()
        }
    }

    fileprivate func setupLoadingHiddenBasedOnState() {
        loadingHidden.value = state != .loading
    }

    fileprivate func setupDataViewHiddenBasedOnState() {
        dataViewHidden.value = state != .success
    }

    fileprivate func setupErrorBasedOnState() {
        if case .error(let message) = state {
            error.value = message
        } else {
            error.value = nil
        }
    }

}
