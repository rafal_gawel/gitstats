//
//  RequestResult.swift
//  Gitstats
//
//  Created by Rafał Gaweł on 28/01/2017.
//  Copyright © 2017 Rafał Gaweł. All rights reserved.
//

import Foundation

enum RequestResult<T> {

    case success(data: T)
    case failure(error: String)

}
