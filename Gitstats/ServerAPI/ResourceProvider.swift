//
//  ResourceProvider.swift
//  Gitstats
//
//  Created by Rafał Gaweł on 31/01/2017.
//  Copyright © 2017 Rafał Gaweł. All rights reserved.
//

import Foundation

typealias FetchUsersCallback = (RequestResult<[User]>) -> Void
typealias FetchReposCallback = (RequestResult<[Repo]>) -> Void
typealias FetchLanguagesCallback = (RequestResult<Languages>) -> Void

protocol ResourceProvider {

    func fetchUsers(callback: @escaping FetchUsersCallback)
    func fetchRepos(username: String, callback: @escaping FetchReposCallback)
    func fetchLanguages(username: String, repo: String, callback: @escaping FetchLanguagesCallback)

}
