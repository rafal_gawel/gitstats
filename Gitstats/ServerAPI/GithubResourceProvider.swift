//
//  GithubResourceProvider.swift
//  Gitstats
//
//  Created by Rafał Gaweł on 29/01/2017.
//  Copyright © 2017 Rafał Gaweł. All rights reserved.
//

import Foundation
import Moya
import Unbox

class GithubResourceProvider: ResourceProvider {

    fileprivate let moyaProvider = MoyaProvider<MoyaGithubProvider>()

    func fetchUsers(callback: @escaping FetchUsersCallback) {
        moyaProvider.request(.fetchUsers) { (result) in
            switch result {
            case .success(let response):
                do {
                    let users: [User] = try unbox(data: response.data)
                    callback(RequestResult.success(data: users))
                } catch {
                    callback(RequestResult.failure(error: "Could not parse server response"))
                }
            case .failure(let error):
                callback(RequestResult.failure(error: error.localizedDescription))
            }
        }
    }

    func fetchRepos(username: String, callback: @escaping FetchReposCallback) {
        moyaProvider.request(.fetchRepos(username: username)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let repos: [Repo] = try unbox(data: response.data)
                    callback(RequestResult.success(data: repos))
                } catch {
                    callback(RequestResult.failure(error: "Could not parse server response"))
                }
            case .failure(let error):
                callback(RequestResult.failure(error: error.localizedDescription))
            }
        }
    }

    func fetchLanguages(username: String, repo: String, callback: @escaping FetchLanguagesCallback) {
        moyaProvider.request(.fetchLanguages(username: username, repo: repo)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let languages: Languages = try unbox(data: response.data)
                    callback(RequestResult.success(data: languages))
                } catch {
                    callback(RequestResult.failure(error: "Could not parse server response"))
                }
            case .failure(let error):
                callback(RequestResult.failure(error: error.localizedDescription))
            }
        }
    }

}
