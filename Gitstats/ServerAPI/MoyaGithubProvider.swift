//
//  MoyaGithubProvider.swift
//  Gitstats
//
//  Created by Rafał Gaweł on 29/01/2017.
//  Copyright © 2017 Rafał Gaweł. All rights reserved.
//

import Foundation
import Moya

enum MoyaGithubProvider {

    case fetchUsers
    case fetchRepos(username: String)
    case fetchLanguages(username: String, repo: String)

}

extension MoyaGithubProvider: TargetType {

    var baseURL: URL {
        return URL(string: "https://api.github.com")!
    }

    var path: String {
        switch self {
        case .fetchUsers:
            return "/users"
        case .fetchRepos(let username):
            return "/users/\(username)/repos"
        case .fetchLanguages(let username, let repo):
            return "/repos/\(username)/\(repo)/languages"
        }
    }
    var method: Moya.Method {
        switch self {
        case .fetchUsers, .fetchRepos, .fetchLanguages:
            return .get
        }
    }
    var parameters: [String: Any]? {
        switch self {
        case .fetchUsers, .fetchRepos, .fetchLanguages:
            return nil
        }
    }

    var parameterEncoding: ParameterEncoding {
        return URLEncoding.default
    }

    var sampleData: Data {
        return Data()
    }

    var task: Task {
        switch self {
        case .fetchUsers, .fetchRepos, .fetchLanguages:
            return .request
        }
    }

    var validate: Bool {
        return false
    }

}
