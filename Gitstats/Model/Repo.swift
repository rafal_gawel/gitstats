//
//  Repo.swift
//  Gitstats
//
//  Created by Rafał Gaweł on 31/01/2017.
//  Copyright © 2017 Rafał Gaweł. All rights reserved.
//

import Foundation
import Unbox

class Repo: Unboxable {

    let name: String

    init(name: String) {
        self.name = name
    }

    required init(unboxer: Unboxer) throws {
        name = try unboxer.unbox(key: "name")
    }

}
