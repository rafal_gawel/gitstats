//
//  Language.swift
//  Gitstats
//
//  Created by Rafał Gaweł on 31/01/2017.
//  Copyright © 2017 Rafał Gaweł. All rights reserved.
//

import Foundation

struct Language {

    let name: String
    let bytesOfCode: Double

}
