//
//  Languages.swift
//  Gitstats
//
//  Created by Rafał Gaweł on 31/01/2017.
//  Copyright © 2017 Rafał Gaweł. All rights reserved.
//

import Foundation
import Unbox

class Languages: Unboxable {

    let items: [Language]

    init(items: [Language]) {
        self.items = items
    }

    required init(unboxer: Unboxer) throws {
        items = unboxer.dictionary.flatMap { (name, bytesOfCode) -> Language? in
            guard let bytesOfCode = bytesOfCode as? Double else {
                return nil
            }

            return Language(name: name, bytesOfCode: bytesOfCode)
        }
    }

}
