//
//  User.swift
//  Gitstats
//
//  Created by Rafał Gaweł on 26/01/2017.
//  Copyright © 2017 Rafał Gaweł. All rights reserved.
//

import Foundation
import Unbox

class User: Unboxable {

    let name: String
    let avatarImageURL: String?

    init(name: String, avatarImageURL: String) {
        self.name = name
        self.avatarImageURL = avatarImageURL
    }

    required init(unboxer: Unboxer) throws {
        name = unboxer.unbox(key: "login") ?? "Noname"
        avatarImageURL = unboxer.unbox(key: "avatar_url")
    }

}
