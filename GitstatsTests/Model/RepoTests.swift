//
//  RepoTests.swift
//  Gitstats
//
//  Created by Rafał Gaweł on 31/01/2017.
//  Copyright © 2017 Rafał Gaweł. All rights reserved.
//

import XCTest
@testable import Gitstats

class RepoTests: XCTestCase {

    func testUnboxJSON() throws {
        let repo: Repo = try JSONTestUtils().parse(file: "repo")

        XCTAssertEqual(repo.name, "30daysoflaptops.github.io")
    }

}
