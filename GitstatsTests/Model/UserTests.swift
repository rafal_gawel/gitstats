//
//  UserTests.swift
//  Gitstats
//
//  Created by Rafał Gaweł on 29/01/2017.
//  Copyright © 2017 Rafał Gaweł. All rights reserved.
//

import XCTest
@testable import Gitstats

class UserTests: XCTestCase {

    func testUnboxJSON() throws {
        let user: User = try JSONTestUtils().parse(file: "user")

        XCTAssertEqual(user.name, "mojombo")
        XCTAssertEqual(user.avatarImageURL, "https://avatars.githubusercontent.com/u/1?v=3")
    }

    func testUnboxJSONFallbackValues() throws {
        let user: User = try JSONTestUtils().parse(file: "empty")

        XCTAssertEqual(user.name, "Noname")
        XCTAssertEqual(user.avatarImageURL, nil)
    }

}
