//
//  LanguagesTests.swift
//  Gitstats
//
//  Created by Rafał Gaweł on 31/01/2017.
//  Copyright © 2017 Rafał Gaweł. All rights reserved.
//

import XCTest
@testable import Gitstats

class LanguagesTests: XCTestCase {

    func testUnboxJSON() throws {
        let languages: Languages = try JSONTestUtils().parse(file: "languages")

        XCTAssertEqual(languages.items.count, 3)
        XCTAssertEqual(languages.items[0].name, "JavaScript")
        XCTAssertEqual(languages.items[0].bytesOfCode, 24107)
        XCTAssertEqual(languages.items[1].name, "CoffeeScript")
        XCTAssertEqual(languages.items[1].bytesOfCode, 2592)
        XCTAssertEqual(languages.items[2].name, "CSS")
        XCTAssertEqual(languages.items[2].bytesOfCode, 242)
    }

    func testUnboxEmptyJSON() throws {
        let languages: Languages = try JSONTestUtils().parse(file: "empty")

        XCTAssertEqual(languages.items.count, 0)
    }

}
