//
//  UserViewModelTests.swift
//  Gitstats
//
//  Created by Rafał Gaweł on 26/01/2017.
//  Copyright © 2017 Rafał Gaweł. All rights reserved.
//

import XCTest
@testable import Gitstats

class UserViewModelTests: XCTestCase {

    func testConstructionWithModel() {
        // given
        let user = User(name: "john", avatarImageURL: "www.gravatar.com/avatar.png")

        // when
        let viewModel = UserViewModel(user: user, resourceProvider: ResourceProviderMock())

        // then
        XCTAssertEqual(viewModel.userName.value, "john")
        XCTAssertEqual(viewModel.avatarImageURL.value, "www.gravatar.com/avatar.png")
    }

    func testEquality() {
        XCTAssertEqual(makeViewModel(name: "john", avatarImageURL: "photo.png", numberOfRepositories: 10),
                       makeViewModel(name: "john", avatarImageURL: "photo.png", numberOfRepositories: 10))

        XCTAssertNotEqual(makeViewModel(name: "john", avatarImageURL: "photo.png", numberOfRepositories: 10),
                          makeViewModel(name: "sara", avatarImageURL: "photo.png", numberOfRepositories: 10))

        XCTAssertNotEqual(makeViewModel(name: "john", avatarImageURL: "photo.png", numberOfRepositories: 10),
                          makeViewModel(name: "john", avatarImageURL: "image.png", numberOfRepositories: 10))
    }

    func testRepoCountText() {
        // given
        let resourceProvider = ResourceProviderMock()
        let user = User(name: "john", avatarImageURL: "www.gravatar.com/avatar.png")
        let viewModel = UserViewModel(user: user, resourceProvider: resourceProvider)

        // when
        viewModel.repoList.fetchIfNotLoaded()
        resourceProvider.fetchReposCallback?(.success(data: [Repo(name: "A"), Repo(name: "B"), Repo(name: "C")]))

        // then
        XCTAssertEqual(viewModel.repoCountText.value, "repos: 3")
    }

    fileprivate func makeViewModel(name: String, avatarImageURL: String, numberOfRepositories: Int) -> UserViewModel {
        let user = User(name: name, avatarImageURL: avatarImageURL)
        return UserViewModel(user: user, resourceProvider: ResourceProviderMock())
    }

}
