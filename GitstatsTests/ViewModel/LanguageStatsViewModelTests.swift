//
//  LanguageStatsViewModelTests.swift
//  Gitstats
//
//  Created by Rafał Gaweł on 31/01/2017.
//  Copyright © 2017 Rafał Gaweł. All rights reserved.
//

import XCTest
@testable import Gitstats

class LanguageStatsViewModelTests: XCTestCase {

    fileprivate var resourceProvider: ResourceProviderMock!
    fileprivate var viewModel: LanguageStatsViewModel!

    override func setUp() {
        super.setUp()

        // given
        resourceProvider = ResourceProviderMock()
        viewModel = LanguageStatsViewModel(username: "john", repo: "hello-world", resourceProvider: resourceProvider)
    }

    func testStartFetch() {
        // when
        viewModel.fetch()

        // then
        XCTAssertEqual(resourceProvider.fetchLanguagesCallCount, 1)
        XCTAssertEqual(resourceProvider.fetchLanguagesUsername, "john")
        XCTAssertEqual(resourceProvider.fetchLanguagesRepo, "hello-world")
        XCTAssertEqual(viewModel.loadingState.state, .loading)
    }

    func testFetchSucceed() {
        // given
        let cpp = Language(name: "C++", bytesOfCode: 100)
        let java = Language(name: "Java", bytesOfCode: 300)
        let languages = Languages(items: [cpp, java])

        // when
        viewModel.fetch()
        resourceProvider.fetchLanguagesCallback?(.success(data: languages))

        // then
        XCTAssertEqual(viewModel.loadingState.state, .success)
        XCTAssertEqual(viewModel.items.value, "C++ 25%, Java 75%")
    }

    func testPopulateWithEmptyLanguages() {
        // given
        let languages = Languages(items: [])

        // when
        viewModel.populate(data: languages)

        // then
        XCTAssertEqual(viewModel.items.value, "")
    }

    func testPopulateWithZeroBytesOfCode() {
        // given
        let rust = Language(name: "Rust", bytesOfCode: 0)
        let go = Language(name: "Go", bytesOfCode: 0)
        let languages = Languages(items: [rust, go])

        // when
        viewModel.populate(data: languages)

        // then
        XCTAssertEqual(viewModel.items.value, "Rust, Go")
    }

    func testPopulateWithOneLanguage() {
        // given
        let ruby = Language(name: "Ruby", bytesOfCode: 300)
        let languages = Languages(items: [ruby])

        // when
        viewModel.populate(data: languages)

        // then
        XCTAssertEqual(viewModel.items.value, "Ruby 100%")
    }

    func testPopulateWithTwoLanguages() {
        // given
        let swift = Language(name: "Swift", bytesOfCode: 200)
        let objc = Language(name: "Objective-C", bytesOfCode: 100)
        let languages = Languages(items: [swift, objc])

        // when
        viewModel.populate(data: languages)

        // then
        XCTAssertEqual(viewModel.items.value, "Swift 67%, Objective-C 33%")
    }

}
