//
//  UserListViewModelTests.swift
//  Gitstats
//
//  Created by Rafał Gaweł on 28/01/2017.
//  Copyright © 2017 Rafał Gaweł. All rights reserved.
//

import XCTest
@testable import Gitstats

class UserListViewModelTests: XCTestCase {

    fileprivate let john = User(name: "john", avatarImageURL: "john_photo.png")
    fileprivate let lucy = User(name: "lucy", avatarImageURL: "lucy_photo.png")

    fileprivate var resourceProvider: ResourceProviderMock!
    fileprivate var viewModel: UserListViewModel!

    override func setUp() {
        super.setUp()

        // given
        resourceProvider = ResourceProviderMock()
        viewModel = UserListViewModel(resourceProvider: resourceProvider)
    }

    func testTitleText() {
        // then
        XCTAssertEqual(viewModel.titleText, "Users")
    }

    func testStartFetch() {
        // when
        viewModel.fetch()

        // then
        XCTAssertEqual(resourceProvider.fetchUsersCallCount, 1)
        XCTAssertEqual(viewModel.loadingState.state, .loading)
    }

    func testFetchSucceed() {
        // when
        viewModel.fetch()
        resourceProvider.fetchUsersCallback?(.success(data: [john, lucy]))

        // then
        XCTAssertEqual(viewModel.loadingState.state, .success)
        if let items = viewModel.items.value {
            XCTAssertEqual(items, [makeVieModel(user: john), makeVieModel(user: lucy)])
        } else {
            XCTFail("Nil viewModel.items.value")
        }
    }

    fileprivate func makeVieModel(user: User) -> UserViewModel {
        return UserViewModel(user: user, resourceProvider: resourceProvider)
    }

}
