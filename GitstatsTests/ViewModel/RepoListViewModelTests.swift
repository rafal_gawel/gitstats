//
//  RepoListViewModelTests.swift
//  Gitstats
//
//  Created by Rafał Gaweł on 31/01/2017.
//  Copyright © 2017 Rafał Gaweł. All rights reserved.
//

import XCTest
@testable import Gitstats

class RepoListViewModelTests: XCTestCase {

    fileprivate let ubuntu = Repo(name: "ubuntu")
    fileprivate let debian = Repo(name: "debian")

    fileprivate var resourceProvider: ResourceProviderMock!
    fileprivate var viewModel: RepoListViewModel!

    override func setUp() {
        super.setUp()

        // given
        resourceProvider = ResourceProviderMock()
        viewModel = RepoListViewModel(username: "john", resourceProvider: resourceProvider)
    }

    func testTitleText() {
        // then
        XCTAssertEqual(viewModel.titleText, "john's repos")
    }

    func testStartFetch() {
        // when
        viewModel.fetch()

        // then
        XCTAssertEqual(resourceProvider.fetchReposCallCount, 1)
        XCTAssertEqual(resourceProvider.fetchReposUsername, "john")
        XCTAssertEqual(viewModel.loadingState.state, .loading)
    }

    func testFetchSucceed() {
        // when
        viewModel.fetch()
        resourceProvider.fetchReposCallback?(.success(data: [ubuntu, debian]))

        // then
        XCTAssertEqual(resourceProvider.fetchReposCallCount, 1)
        XCTAssertEqual(viewModel.loadingState.state, .success)
        XCTAssertEqual(viewModel.items.value ?? [], [makeVieModel(repo: ubuntu), makeVieModel(repo: debian)])
    }

    fileprivate func makeVieModel(repo: Repo) -> RepoViewModel {
        return RepoViewModel(username: "john", repo: repo.name, resourceProvider: resourceProvider)
    }

}
