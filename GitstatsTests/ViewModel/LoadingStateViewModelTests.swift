//
//  LoadingStateViewModelTests.swift
//  Gitstats
//
//  Created by Rafał Gaweł on 30/01/2017.
//  Copyright © 2017 Rafał Gaweł. All rights reserved.
//

import XCTest
@testable import Gitstats

class LoadingStateViewModelTests: XCTestCase {

    fileprivate var viewModel: LoadingStateViewModel!

    fileprivate var loadingHiddenStates: String = ""
    fileprivate var dataViewHiddenStates: String = ""
    fileprivate var errorStates: String = ""

    override func setUp() {
        super.setUp()

        // given
        viewModel = LoadingStateViewModel()

        loadingHiddenStates = ""
        dataViewHiddenStates = ""
        errorStates = ""
    }

    func testInitialState() {
        // when
        dumpStates()

        // then
        XCTAssertEqual(loadingHiddenStates, "T")
        XCTAssertEqual(dataViewHiddenStates, "T")
        XCTAssertEqual(errorStates, ".")
    }

    func testChangeStateToNotStarted() {
        // when
        dumpStateAfterTransition(from: .notStarted, to: .notStarted)
        dumpStateAfterTransition(from: .loading, to: .notStarted)
        dumpStateAfterTransition(from: .success, to: .notStarted)
        dumpStateAfterTransition(from: .error(message: "X"), to: .notStarted)

        // then
        XCTAssertEqual(loadingHiddenStates, "TTTT")
        XCTAssertEqual(dataViewHiddenStates, "TTTT")
        XCTAssertEqual(errorStates, "....")
    }

    func testChangeStateToLoading() {
        // when
        dumpStateAfterTransition(from: .notStarted, to: .loading)
        dumpStateAfterTransition(from: .loading, to: .loading)
        dumpStateAfterTransition(from: .success, to: .loading)
        dumpStateAfterTransition(from: .error(message: "X"), to: .loading)

        // then
        XCTAssertEqual(loadingHiddenStates, "FFFF")
        XCTAssertEqual(dataViewHiddenStates, "TTTT")
        XCTAssertEqual(errorStates, "....")
    }

    func testChangeStateToSuccess() {
        // when
        dumpStateAfterTransition(from: .notStarted, to: .success)
        dumpStateAfterTransition(from: .loading, to: .success)
        dumpStateAfterTransition(from: .success, to: .success)
        dumpStateAfterTransition(from: .error(message: "X"), to: .success)

        // then
        XCTAssertEqual(loadingHiddenStates, "TTTT")
        XCTAssertEqual(dataViewHiddenStates, "FFFF")
        XCTAssertEqual(errorStates, "....")
    }

    func testChangeStateToError() {
        // when
        dumpStateAfterTransition(from: .notStarted, to: .error(message: "E"))
        dumpStateAfterTransition(from: .loading, to: .error(message: "E"))
        dumpStateAfterTransition(from: .success, to: .error(message: "E"))
        dumpStateAfterTransition(from: .error(message: "X"), to: .error(message: "E"))

        // then
        XCTAssertEqual(loadingHiddenStates, "TTTT")
        XCTAssertEqual(dataViewHiddenStates, "TTTT")
        XCTAssertEqual(errorStates, "EEEE")
    }

    fileprivate func dumpStateAfterTransition(from state: LoadingState, to nextState: LoadingState) {
        viewModel.state = state
        viewModel.state = nextState
        dumpStates()
    }

    fileprivate func dumpStates() {
        loadingHiddenStates += viewModel.loadingHidden.value ? "T" : "F"
        dataViewHiddenStates += viewModel.dataViewHidden.value ? "T" : "F"
        errorStates += viewModel.error.value ?? "."
    }

}
