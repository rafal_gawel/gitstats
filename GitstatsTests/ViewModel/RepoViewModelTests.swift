//
//  RepoViewModelTests.swift
//  Gitstats
//
//  Created by Rafał Gaweł on 31/01/2017.
//  Copyright © 2017 Rafał Gaweł. All rights reserved.
//

import XCTest
@testable import Gitstats

class RepoViewModelTests: XCTestCase {

    func testConstructionWithModel() {
        // given
        let resourceProvider = ResourceProviderMock()
        let repo = Repo(name: "gitstats")

        // when
        let viewModel = RepoViewModel(username: "john", repo: repo.name, resourceProvider: resourceProvider)

        // then
        XCTAssertEqual(viewModel.name.value, "gitstats")
    }

    func testEquality() {
        XCTAssertEqual(makeViewModel(name: "ubuntu"), makeViewModel(name: "ubuntu"))
        XCTAssertNotEqual(makeViewModel(name: "ubuntu"), makeViewModel(name: "debian"))
    }

    fileprivate func makeViewModel(name: String) -> RepoViewModel {
        let resourceProvider = ResourceProviderMock()
        let repo = Repo(name: name)
        return RepoViewModel(username: "john", repo: repo.name, resourceProvider: resourceProvider)
    }

}
