//
//  ResourceViewModelTests.swift
//  Gitstats
//
//  Created by Rafał Gaweł on 31/01/2017.
//  Copyright © 2017 Rafał Gaweł. All rights reserved.
//

import XCTest
@testable import Gitstats

class ResourceViewModelTests: XCTestCase {

    class TestingResourceViewModel: ResourceViewModel<String, String> {

        var startFetchingCallCount: Int = 0

        override func startFetching() {
            startFetchingCallCount += 1
        }

        override func populate(data: String) {
            items.value = data + " view model"
        }

    }

    fileprivate var viewModel: TestingResourceViewModel!

    override func setUp() {
        super.setUp()

        // given
        viewModel = TestingResourceViewModel()
    }

    func testFetchStarted() {
        // when
        viewModel.fetch()

        // then
        XCTAssertEqual(viewModel.startFetchingCallCount, 1)
        XCTAssertEqual(viewModel.items.value, nil)
        XCTAssertEqual(viewModel.loadingState.state, LoadingState.loading)
    }

    func testFetchSucceed() {
        // when
        viewModel.fetch()
        viewModel.fetchCallback(result: .success(data: "A"))

        // then
        XCTAssertEqual(viewModel.items.value, "A view model")
        XCTAssertEqual(viewModel.loadingState.state, LoadingState.success)
    }

    func testFetchFailed() {
        // when
        viewModel.fetch()
        viewModel.fetchCallback(result: .failure(error: "No internet connection"))

        // then
        XCTAssertEqual(viewModel.loadingState.state, LoadingState.error(message: "No internet connection"))
    }

    func testFirstFetchSucceedSecondStarted() {
        // when
        viewModel.fetch()
        viewModel.fetchCallback(result: .success(data: "A"))
        viewModel.fetch()

        // then
        XCTAssertEqual(viewModel.startFetchingCallCount, 2)
        XCTAssertEqual(viewModel.items.value, nil)
        XCTAssertEqual(viewModel.loadingState.state, LoadingState.loading)
    }

    func testFirstFetchFailedSecondStarted() {
        // when
        viewModel.fetch()
        viewModel.fetchCallback(result: .failure(error: "No internet connection"))
        viewModel.fetch()

        // then
        XCTAssertEqual(viewModel.startFetchingCallCount, 2)
        XCTAssertEqual(viewModel.loadingState.state, LoadingState.loading)
    }

    func testSecondFetchIsIgnoredWhenFirstIsNotFinished() {
        // when
        viewModel.fetch()
        viewModel.fetch()

        // then
        XCTAssertEqual(viewModel.startFetchingCallCount, 1)
        XCTAssertEqual(viewModel.loadingState.state, LoadingState.loading)
    }

    func testFetchOrIgnoreIfLoaded() {
        // when
        viewModel.fetchIfNotLoaded()
        viewModel.fetchCallback(result: .success(data: "A"))
        viewModel.fetchIfNotLoaded()

        // then
        XCTAssertEqual(viewModel.startFetchingCallCount, 1)
        XCTAssertEqual(viewModel.loadingState.state, .success)
        XCTAssertEqual(viewModel.items.value, "A view model")
    }

}
