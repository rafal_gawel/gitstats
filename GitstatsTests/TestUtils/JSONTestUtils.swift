//
//  JSONTestUtils.swift
//  Gitstats
//
//  Created by Rafał Gaweł on 29/01/2017.
//  Copyright © 2017 Rafał Gaweł. All rights reserved.
//

import Foundation
@testable import Unbox

class JSONTestUtils {

    func parse<T: Unboxable>(file: String) throws -> T {
        let bundle = Bundle(for: type(of: self))
        let url = bundle.url(forResource: file, withExtension: "json")

        let data = try Data(contentsOf: url!)
        return try unbox(data: data)
    }

}
