//
//  ResourceProviderMock.swift
//  Gitstats
//
//  Created by Rafał Gaweł on 29/01/2017.
//  Copyright © 2017 Rafał Gaweł. All rights reserved.
//

import Foundation
@testable import Gitstats

class ResourceProviderMock: ResourceProvider {

    var fetchUsersCallCount: Int = 0
    var fetchUsersCallback: FetchUsersCallback?

    var fetchReposCallCount: Int = 0
    var fetchReposCallback: FetchReposCallback?
    var fetchReposUsername: String?

    var fetchLanguagesCallCount: Int = 0
    var fetchLanguagesCallback: FetchLanguagesCallback?
    var fetchLanguagesUsername: String?
    var fetchLanguagesRepo: String?

    func fetchUsers(callback: @escaping FetchUsersCallback) {
        fetchUsersCallCount += 1
        fetchUsersCallback = callback
    }

    func fetchRepos(username: String, callback: @escaping FetchReposCallback) {
        fetchReposUsername = username
        fetchReposCallCount += 1
        fetchReposCallback = callback
    }

    func fetchLanguages(username: String, repo: String, callback: @escaping FetchLanguagesCallback) {
        fetchLanguagesUsername = username
        fetchLanguagesRepo = repo
        fetchLanguagesCallCount += 1
        fetchLanguagesCallback = callback
    }

}
